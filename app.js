'use strict';

var express = require('express'),
    server  = express(),
    port    = process.env.PORT || 5000;

function logit(path) {
    // We should really be using a logger module
    console.log('%s - GET %s', (new Date()).toLocaleString(), path);
}

server.get('/_healthcheck', function getHealthCheck(req, res) {
    res.send('OK');
});

server.get('/', function getHome(req, res) {
    res.send('Hello World');
    logit(req.path);
});

server.get('/:name', function getHello(req, res) {
    res.send('Hello ' + req.params.name);
    logit(req.path);
});

server.listen(port, function () {
    console.log('Hello World now running on port %s', port);
});
